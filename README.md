# RIOT course exercise applications

This repository contains the exercise applications developed during the RIOT
course.

## License

All sources of the application are license under the LGPL-v2.1